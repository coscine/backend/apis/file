﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.File
{ 
    public class Program : AbstractProgram<ConsulConfiguration>
    {

        public static void Main()
        {
            InitializeWebService<Startup>();
        }
    }
}

