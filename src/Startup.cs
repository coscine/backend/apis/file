﻿using Coscine.Api.File.Services;
using Coscine.ApiCommons;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Coscine.Api.File
{
    public class Startup : AbstractStartup
    {
        public Startup()
        {
        }

        public override void ConfigureServicesExtension(IServiceCollection services)
        {
            base.ConfigureServicesExtension(services);

            services.AddHttpClient<IDataSourceService, DataSourceService>(client =>
            {
                // XXX: Discuss Timeout value
                client.Timeout = TimeSpan.FromMinutes(30);
            });
        }

    }
}
