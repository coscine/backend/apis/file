﻿using Coscine.Database.DataModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Coscine.Database.Util;
using Coscine.ApiCommons.Utils;
using Coscine.ApiCommons;
using Coscine.Database.Models;
using Coscine.Configuration;
using Coscine.Database.ReturnObjects;
using Coscine.ApiCommons.Factories;
using System.Web;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Coscine.Logging;
using Coscine.Api.File.Services;

namespace Coscine.Api.File.Controllers
{
    [Authorize]
    public class DataSourceController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly JWTHandler _jwtHandler;
        private readonly Authenticator _authenticator;
        private readonly ResourceModel _resourceModel;
        private readonly string _accessKey;
        private readonly string _secretKey;
        private readonly AmazonS3Config _amazonConfig;
        private readonly ProjectResourceModel _projectResourceModel;
        private readonly ProjectRoleModel _projectRoleModel;
        private readonly CoscineLogger _coscineLogger;
        private readonly AnalyticsLogObject _analyticsLogObject;
        private readonly IDataSourceService _dataSourceService;
        private readonly string _waterbuterUrl;
        private readonly string _rdsResourceHost;

        public DataSourceController(ILogger<DataSourceController> logger, IDataSourceService dataSourceService)
        {
            _configuration = Program.Configuration;
            _jwtHandler = new JWTHandler(_configuration);
            _authenticator = new Authenticator(this, _configuration);
            _resourceModel = new ResourceModel();
            _projectResourceModel = new ProjectResourceModel();
            _projectRoleModel = new ProjectRoleModel();

            _accessKey = _configuration.GetStringAndWait("coscine/global/ecs/object_user_name");
            _secretKey = _configuration.GetStringAndWait("coscine/global/ecs/object_user_secretkey");
            _rdsResourceHost = _configuration.GetString("coscine/global/ecs/s3_endpoint");
            _waterbuterUrl = _configuration.GetString("coscine/global/waterbutler_url");

            _coscineLogger = new CoscineLogger(logger);
            _analyticsLogObject = new AnalyticsLogObject();
            _amazonConfig = new AmazonS3Config
            {
                ServiceURL = _rdsResourceHost,
                ForcePathStyle = true
            };
            _dataSourceService = dataSourceService;
        }

        [HttpGet("[controller]/{resourceId}/quota")]
        public IActionResult GetQuota(string resourceId)
        {
            if (!Guid.TryParse(resourceId, out Guid resourceGuid))
            {
                return BadRequest($"{resourceId} is not a guid.");
            }

            var user = _authenticator.GetUser();
            Resource resource;
            try
            {
                resource = _resourceModel.GetById(resourceGuid);
                if (resource == null)
                {
                    return NotFound($"Could not find resource with id: {resourceId}");
                }
            }
            catch (Exception)
            {
                return NotFound($"Could not find resource with id: {resourceId}");
            }

            if (resource.Type == null)
            {
                ResourceTypeModel resourceTypeModel = new ResourceTypeModel();
                resource.Type = resourceTypeModel.GetById(resource.TypeId);
            }

            if (!_resourceModel.HasAccess(user, resource, UserRoles.Owner, UserRoles.Member))
            {
                return BadRequest("User does not have permission to the resource.");
            }

            if (resource.Type.DisplayName.ToLower() == "rds" && resource.ResourceTypeOptionId.HasValue)
            {
                var rdsResourceTypeModel = new RDSResourceTypeModel();
                var rdsResourceType = rdsResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                var bucketname = rdsResourceType.BucketName;

                using (var client = new AmazonS3Client(_accessKey, _secretKey, _amazonConfig))
                {
                    long totalFileSize = 0;
                    long fileCount = 0;
                    ListObjectsRequest listReqeust = new ListObjectsRequest()
                    {
                        BucketName = bucketname
                    };

                    ListObjectsResponse listResponse;
                    try
                    {
                        do
                        {
                            listResponse = client.ListObjects(listReqeust);
                            fileCount += listResponse.S3Objects.Count();
                            totalFileSize += listResponse.S3Objects.Sum(x => x.Size);
                            listReqeust.Marker = listResponse.NextMarker;

                        } while (listResponse.IsTruncated);
                    }
                    catch (Exception)
                    {
                        return StatusCode(500);
                    }

                    return Ok($"{{ \"data\": {{ \"fileCount\": {fileCount}, \"usedSizeByte\": {totalFileSize} }}}}");
                }
            }
            else
            {
                return BadRequest("The resource type must be rds.");
            }
        }
        // inferring a ../ (urlencoded) can manipulate the url.
        // However the constructed signature for s3 won't match and it will not be resolved.
        // This may be a problem for other provider!
        [HttpGet("[controller]/{resourceId}/{path}")]
        public async Task<IActionResult> GetWaterButlerFolder(string resourceId, string path)
        {
            var user = _authenticator.GetUser();

            path = FormatPath(path);

            var check = CheckResourceIdAndPath(resourceId, path, out Resource resource);
            if (check != null)
            {
                return check;
            }

            if (!_resourceModel.HasAccess(user, resource, UserRoles.Owner, UserRoles.Member))
            {
                return BadRequest("User does not have permission to the resource.");
            }

            var authHeader = BuildAuthHeader(resource);

            if (authHeader == null)
            {
                return BadRequest($"No provider for: \"{resource.Type.DisplayName}\".");
            }
            else
            {
                // If the path is null, an empty string is added.
                string url = $"{_waterbuterUrl}{(GetResourceTypeName(resource).ToLower() == "s3" ? "rds" : GetResourceTypeName(resource).ToLower())}{path}";
                var response = await _dataSourceService.GetWithHeader(url, authHeader);
                if (response.IsSuccessStatusCode)
                {
                    if (response.Content.Headers.Contains("Content-Disposition"))
                    {
                        LogAnalytics("Download File", resourceId, path.Substring(1), user);
                        return File(await response.Content.ReadAsStreamAsync(),
                            response.Content.Headers.GetValues("Content-Type").First());
                    }
                    else
                    {
                        LogAnalytics("View RCV", resourceId, path.Substring(1), user);
                        var data = JObject.Parse(await response.Content.ReadAsStringAsync())["data"];
                        return Ok(new WaterbutlerObject(path, data));
                    }
                }
                else
                {
                    return FailedRequest(response, path);
                }
            }
        }

        // inferring a ../ (urlencoded) can manipulate the url.
        // However the constructed signature for s3 won't match and it will not be resolved.
        // This may be a problem for other provider!
        [HttpPut("[controller]/{resourceId}/{path}")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> PutUploadFile(string resourceId, string path)
        {
            var user = _authenticator.GetUser();

            path = FormatPath(path);

            var check = CheckResourceIdAndPath(resourceId, path, out Resource resource);
            if (check != null)
            {
                return check;
            }

            if (!_resourceModel.HasAccess(user, resource, UserRoles.Owner, UserRoles.Member))
            {
                return BadRequest("User does not have permission to the resource.");
            }

            var authHeader = BuildAuthHeader(resource, "gitlab");

            if (authHeader == null)
            {
                return BadRequest($"No provider for: \"{resource.Type.DisplayName}\".");
            }
            else
            {
                // If the path is null, an empty string is added.
                string url = $"{_waterbuterUrl}{(GetResourceTypeName(resource).ToLower() == "s3" ? "rds" : GetResourceTypeName(resource).ToLower())}/?kind=file&name={path}";

                try
                {
                    using (var response = await _dataSourceService.PutWithHeader(url, authHeader, Request.Body))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            LogAnalytics("Upload File", resourceId, path, user);
                            return NoContent();
                        }
                        else
                        {
                            return FailedRequest(response, path);
                        }
                    }
                }
                catch (Exception e)
                {
                    _coscineLogger.Log(e);
                    return BadRequest(e);
                }
            }
        }

        // inferring a ../ (urlencoded) can manipulate the url.
        // However the constructed signature for s3 won't match and it will not be resolved.
        // This may be a problem for other provider!
        [HttpPut("[controller]/{resourceId}/{path}/update")]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> PutUpdateFile(string resourceId, string path)
        {
            var user = _authenticator.GetUser();

            path = FormatPath(path);

            var check = CheckResourceIdAndPath(resourceId, path, out Resource resource);
            if (check != null)
            {
                return check;
            }

            if (!_resourceModel.HasAccess(user, resource, UserRoles.Owner, UserRoles.Member))
            {
                return BadRequest("User does not have permission to the resource.");
            }

            var authHeader = BuildAuthHeader(resource, "gitlab");

            if (authHeader == null)
            {
                return BadRequest($"No provider for: \"{resource.Type.DisplayName}\".");
            }
            else
            {
                // If the path is null, an empty string is added.
                string url = $"{_waterbuterUrl}{(GetResourceTypeName(resource).ToLower() == "s3" ? "rds" : GetResourceTypeName(resource).ToLower())}/{path}?kind=file";

                try
                {
                    using (var response = await _dataSourceService.PutWithHeader(url, authHeader, Request.Body))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            LogAnalytics("Update File", resourceId, path, user);
                            return NoContent();
                        }
                        else
                        {
                            return FailedRequest(response, path);
                        }
                    }
                }
                catch (Exception e)
                {
                    _coscineLogger.Log(e);
                    return BadRequest(e);
                }
            }
        }

        private string FormatPath(string path)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                path = HttpUtility.UrlDecode(path);
                path = path.Replace(@"\", "/");
            }

            return path;
        }

        private string GetResourceTypeName(Resource resource)
        {
            if (resource.Type.DisplayName.ToLower().Equals("s3"))
            {
                return "rds";
            }
            else
            {
                return resource.Type.DisplayName.ToLower();
            }
        }

        private string GetResourceTypeName(JToken resource)
        {
            if (resource["type"]["displayName"].ToString().ToLower().Equals("s3"))
            {
                return "rds";
            }
            else
            {
                return resource["type"]["displayName"].ToString().ToLower();
            }
        }


        

        [HttpDelete("[controller]/{resourceId}/{path}")]
        public async Task<IActionResult> Delete(string resourceId, string path)
        {
            var user = _authenticator.GetUser();

            path = FormatPath(path);

            var check = CheckResourceIdAndPath(resourceId, path, out Resource resource);
            if (check != null)
            {
                return check;
            }

            if (!_resourceModel.HasAccess(user, resource, UserRoles.Owner, UserRoles.Member))
            {
                return BadRequest("User does not have permission to the resource.");
            }

            var authHeader = BuildAuthHeader(resource, "gitlab");

            if (authHeader == null)
            {
                return BadRequest($"No provider for: \"{resource.Type.DisplayName}\".");
            }
            else
            {
                // If the path is null, an empty string is added.
                string url = $"{_waterbuterUrl}{(GetResourceTypeName(resource).ToLower() == "s3" ? "rds" : GetResourceTypeName(resource).ToLower())}{path}";

                // Thread safe according to msdn and HttpCompletionOption sets it to get only headers first.
                try
                {
                    using (var response = await _dataSourceService.DeleteWithHeader(url, authHeader))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            LogAnalytics("Delete File", resourceId, path, user);
                            return NoContent();
                        }
                        else
                        {
                            return FailedRequest(response, path);
                        }
                    }
                }
                catch (Exception e)
                {
                    _coscineLogger.Log(e);
                    return BadRequest(e);
                }
            }
        }

        [HttpPost("[controller]/validate")]
        public async Task<IActionResult> IsResourceValid()
        {
            var path = "/";

            JToken resource = ObjectFactory<JToken>.DeserializeFromStream(Request.Body);

            string authHeader = null;
            if (resource["type"]["displayName"].ToString().ToLower() == "s3")
            {
                S3ResourceType s3ResourceType = new S3ResourceType
                {
                    BucketName = resource["resourceTypeOption"]["BucketName"].ToString(),
                    AccessKey = resource["resourceTypeOption"]["AccessKey"].ToString(),
                    SecretKey = resource["resourceTypeOption"]["SecretKey"].ToString(),
                    ResourceUrl = resource["resourceTypeOption"]["ResourceUrl"].ToString()
                };
                authHeader = BuildS3AuthHeader(s3ResourceType);
            }
            else if (resource["type"]["displayName"].ToString().ToLower() == "gitlab")
            {
                GitlabResourceType gitlabResourceType = new GitlabResourceType
                {
                    RepositoryNumber = (int)resource["resourceTypeOption"]["RepositoryNumber"],
                    RepositoryUrl = resource["resourceTypeOption"]["RepositoryUrl"].ToString(),
                    Token = resource["resourceTypeOption"]["Token"].ToString()
                };
                authHeader = BuildGitlabAuthHeader(gitlabResourceType);
            }

            if (authHeader == null)
            {
                return BadRequest($"No provider for: \"{resource["type"]["displayName"].ToString()}\".");
            }
            else
            {
                // If the path is null, an empty string is added.
                string url = $"{_waterbuterUrl}{(GetResourceTypeName(resource).ToLower() == "s3" ? "rds" : GetResourceTypeName(resource).ToLower())}{path}";

                // Thread safe according to msdn and HttpCompletionOption sets it to get only headers first.
                using (var response = await _dataSourceService.GetWithHeader(url, authHeader))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (response.Content.Headers.Contains("Content-Disposition"))
                        {
                            return File(await response.Content.ReadAsStreamAsync(),
                                response.Content.Headers.GetValues("Content-Type").First());
                        }
                        else
                        {
                            var data = JObject.Parse(await response.Content.ReadAsStringAsync())["data"];
                            return Ok(new WaterbutlerObject(path, data));
                        }
                    }
                    else
                    {
                        return FailedRequest(response, path);
                    }
                }
            }
        }

        private IActionResult FailedRequest(HttpResponseMessage response, string path)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return NotFound($"Could not find object for: \"{path}\".");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                return Forbid("Not allowed to access the datasource.");
            }
            else
            {
                return BadRequest($"Error in communication with waterbutler: {response.StatusCode}");
            }
        }

        private IActionResult CheckResourceIdAndPath(string resourceId, string path, out Resource resource)
        {
            resource = null;

            if (string.IsNullOrWhiteSpace(path))
            {
                return BadRequest($"Your path \"{path}\" is empty.");
            }

            Regex rgx = new Regex(@"[\:?*<>|]+");
            if (rgx.IsMatch(path))
            {
                return BadRequest($"Your path \"{path}\" contains bad characters. The following characters are not permissible: {@"\/:?*<>|"}.");
            }

            if (!Guid.TryParse(resourceId, out Guid resourceGuid))
            {
                return BadRequest($"{resourceId} is not a guid.");
            }

            try
            {
                resource = _resourceModel.GetById(resourceGuid);
                if (resource == null)
                {
                    return NotFound($"Could not find resource with id: {resourceId}");
                }
            }
            catch (Exception)
            {
                return NotFound($"Could not find resource with id: {resourceId}");
            }

            if (resource.Type == null)
            {
                ResourceTypeModel resourceTypeModel = new ResourceTypeModel();
                resource.Type = resourceTypeModel.GetById(resource.TypeId);
            }

            // All good
            return null;
        }

        private string BuildWaterbutlerPayload(Dictionary<string, object> auth, Dictionary<string, object> credentials, Dictionary<string, object> settings)
        {
            var data = new Dictionary<string, object>
            {
                { "auth", auth },
                { "credentials", credentials },
                { "settings", settings },
                { "callback_url", "rwth-aachen.de" }
            };

            var payload = new JwtPayload
            {
                { "data", data }
            };

            return _jwtHandler.GenerateJwtToken(payload);
        }

        private string BuildAuthHeader(Resource resource, params string[] exclude)
        {
            if (exclude != null && exclude.Contains(resource.Type.DisplayName.ToLower()))
            {
                return null;
            }

            string authHeader = null;
            if (resource.Type.DisplayName.ToLower() == "rds")
            {
                RDSResourceTypeModel rdsResourceTypeModel = new RDSResourceTypeModel();
                 var rdsResourceType = rdsResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);

                authHeader = BuildRdsAuthHeader(rdsResourceType);
            }
            else if (resource.Type.DisplayName.ToLower() == "s3")
            {
                S3ResourceTypeModel s3ResourceTypeModel = new S3ResourceTypeModel();
                var s3ResourceType = s3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);

                authHeader = BuildS3AuthHeader(s3ResourceType);
            }
            else if (resource.Type.DisplayName.ToLower() == "gitlab")
            {
                GitlabResourceTypeModel gitlabResourceTypeModel = new GitlabResourceTypeModel();
                var gitlabResourceType = gitlabResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);

                authHeader = BuildGitlabAuthHeader(gitlabResourceType);
            }

            return authHeader;
         }

        private string BuildRdsAuthHeader(RDSResourceType rdsResourceType)
        {
            var auth = new Dictionary<string, object>();
            
            var credentials = new Dictionary<string, object>
            {
                { "access_key", _configuration.GetStringAndWait("coscine/global/ecs/object_user_name") },
                { "secret_key", _configuration.GetStringAndWait("coscine/global/ecs/object_user_secretkey") }
            };

            var settings = new Dictionary<string, object>
            {
                { "bucket", rdsResourceType.BucketName },
                { "host", _rdsResourceHost}
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        private string BuildS3AuthHeader(S3ResourceType s3ResourceType)
        {
            var auth = new Dictionary<string, object>();

            var credentials = new Dictionary<string, object>
            {
                { "access_key", s3ResourceType.AccessKey },
                { "secret_key", s3ResourceType.SecretKey }
            };

            var settings = new Dictionary<string, object>
            {
                { "bucket", s3ResourceType.BucketName },
                { "host", s3ResourceType.ResourceUrl }
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        private string BuildGitlabAuthHeader(GitlabResourceType gitlabResourceType)
        {

            var auth = new Dictionary<string, object>();

            var credentials = new Dictionary<string, object>
            {
                { "token", gitlabResourceType.Token }
            };

            var settings = new Dictionary<string, object>
            {
                { "owner", "Tester"},
                { "repo", gitlabResourceType.RepositoryUrl},
                { "repo_id", gitlabResourceType.RepositoryNumber.ToString()},
                { "host", "https://git.rwth-aachen.de"}
            };

            return BuildWaterbutlerPayload(auth, credentials, settings);
        }

        // XXX extract in the future to an analytics Controller
        private void LogAnalytics(string operation, string resourceId, string path, User user)
        {
            if (CoscineLoggerConfiguration.IsLogLevelActivated(LogType.Analytics))
            {
                _analyticsLogObject.Type = "Action";
                _analyticsLogObject.FileId = resourceId + "/" + path;
                _analyticsLogObject.ResourceId = resourceId;
                _analyticsLogObject.ProjectId = _projectResourceModel.GetProjectForResource(new Guid(resourceId)).ToString();
                _analyticsLogObject.RoleId = _projectRoleModel.GetGetUserRoleForProject(new Guid(_analyticsLogObject.ProjectId), user.Id).ToString();
                _analyticsLogObject.Operation = operation;
                _coscineLogger.AnalyticsLog(_analyticsLogObject);
            }
        }
    }
}
