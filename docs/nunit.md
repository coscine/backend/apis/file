## Add NUnit Tests

### Add Nunit test adapter
1.  Choose **Tools > Extensions and Updates**
2.  In the **Extensions and Updates** dialog box, expand the **Online** category and then **Visual Studio Marketplace**. Then, choose **Tools > Testing**.
2.  Select the **NUnit test adapter** and then choose **Download.**
### Add Tests

1.  Create a class library project and add it to your solution.

    For convinience name it like your main project and add **.Tests** as a suffix.
    ![alt text](images/create_class_library.png "Select Class Library")

2.  Install the plug-in. In **Solution Explorer**, select the class library project, and then choose **Manage NuGet Packages** from its right-click or context menu.

    ![alt text](images/add_a_package.png "Add a package")

3.  In the **NuGet Package Manager** window, search for and select the plug-in (NUnit), and then choose **Install**.
    ![alt text](images/select_framework.png "Select Project")

    The framework is now referenced in your project under **References**.

4.  From the class library project's **References** node, select **Add Reference**.

    ![alt text](images/add_a_reference.png "Add a reference")

5. In the **Reference Manager** dialog box, select the project that contains the code you'll test.

    ![alt text](images/select_project.png "Select Project")

6. Code your unit test.
    
    ![alt text](images/test_code.png "Test Code")
